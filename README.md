## Lectura de una Fotorresistencia

En este repositorio encontraras información sobre como usar una fotorresistencia, para medir intensidad de luz en el ambiente y mostrarlo por el monitor serie de Arduino.

## Materiales

* **Una placa Arduino**
* **Una fotoresistencia**
* **Una resistencia de 10k**
* **Un Protoboard**

## Uso

```
En la carpeta diagrams encontraras la conexion del circuito.
```

```
En la carpeta src encontraras el codigo fuente de ejemplo.
```

```
Una vez compilado el codigo en el IDE de arduino ve a Herramientas --> Monitor Serial
```

Una vez activado el monitor serial nos mostrara información sobre la fotoresistencia.

## Autor

* **Edermar Dominguez** - [Ederdoski](https://github.com/ederdoski)

## License

This code is open-sourced software licensed under the [MIT license.](https://opensource.org/licenses/MIT)

