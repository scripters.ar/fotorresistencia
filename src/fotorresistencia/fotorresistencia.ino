int fotorresistencia = A5;
int value;

void setup () {
  Serial.begin(9600);
  pinMode(fotorresistencia,INPUT);
}

void loop() {
  value = analogRead(fotorresistencia); //--Realizamos la lectura analogica del sensor
  Serial.println(value);                //--Imprimimos el valor por serial
}
